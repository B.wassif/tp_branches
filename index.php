<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Todo List</title>
    <!-- Liens vers les fichiers Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Todo List</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <!-- Formulaire d'ajout de tâche -->
        <form method="post" class="form-inline">
            <div class="form-group">
                <label for="title">Ajouter une tâche :</label>
                <input type="text" class="form-control" id="title" name="title">
            </div>
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </form>
        <br>
        <!-- Liste des tâches -->
        <ul class="list-group">
            <?php foreach($taches as $tache): ?>
                <?php if($tache['done']): ?>
                    <li class="list-group-item list-group-item-success">

                <?php else: ?>
                    <li class="list-group-item list-group-item-warning">
                <?php endif; ?>
                        <?php echo $tache['title']; ?>
                        <!-- Formulaire pour supprimer une tâche -->
                        <form method="post" class="pull-right">
                            <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                            <button type="submit" name="action" value="delete" class="btn btn-danger">Supprimer</button>
                            <!-- Formulaire pour basculer l'état d'une tâche -->
                            <button type="submit" name="action" value="toggle" class="btn btn-primary"><?php echo $tache['done'] ? 'Non fait' : 'Fait'; ?></button>
                        </form>
                    </li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>
</html>

<?php
// Connexion à la base de données
$pdo = new PDO('mysql:host=localhost;dbname=todolist', 'root', '');

// Vérification si un formulaire a été soumis
if(isset($_POST['title'])) {
    // Ajout d'une nouvelle tâche
    $title = $_POST['title'];
    $date = date('Y-m-d H:i:s');
    $stmt = $pdo->prepare('INSERT INTO todo (title, created_at) VALUES (:title, :created_at)');
    $stmt->execute(array(
        ':title' => $title,
        ':created_at' => $date
    ));
}

// Suppression d'une tâche
if(isset($_POST['action']) && $_POST['action'] == 'delete') {
    $id = $_POST['id'];
    $stmt = $pdo->prepare('DELETE FROM todo WHERE id = :id');
    $stmt->execute(array(
        ':id' => $id
    ));
}

// Validation d'une tâche
if(isset($_POST['action']) && $_POST['action'] == 'toggle') {
    $id = $_POST['id'];
    $stmt = $pdo->prepare('UPDATE todo SET done = NOT done WHERE id = :id');
    $stmt->execute(array(
        ':id' => $id
    ));
}

// Récupération de la liste des tâches
$stmt = $pdo->prepare('SELECT * FROM todo ORDER BY created_at DESC');
$stmt->execute();
$taches = $stmt->fetchAll();
?>
